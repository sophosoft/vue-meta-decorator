# vue-meta-decorator

Vue Component classes can now populate meta data with class properties and methods using the `@Meta` decorator.

## installation
```sh
npm i @sophosoft/vue-meta-decorator
# or
yarn add @sophosoft/vue-meta-decorator
```

## setup
Install the peer dependencies:
```sh
yarn add vue-meta vue-property-decorator
```
Then, since we're using custom decorators, ensure that they're enabled in `tsconfig.json`:
```json
{
    "compilerOptions": {
        "experimentalDecorators": true
    }
}
```
Lastly, ensure that Vue is using the `vue-meta` plugin:
```ts
// main.ts
import Vue from 'vue'
import VueMeta from 'vue-meta'

Vue.use(VueMeta)
// ...
```

# usage
Normally, `metaInfo` is configured outside of the class:
```ts
@Component({
    metaInfo: {
        title: 'foo'
    }
})
export default class Foo extends Vue {
    // ...
}
```
However, this is not possible if we want `metaInfo` to use properties and methods internal to our class.  In that case, use this decorator to annotate a method to build your meta data.
```ts
import { Meta } from '@sophosoft/vue-meta-decorator'

@Component({})
export default class Bar extends Vue {
    @Meta
    getMetaInfo() {
        return {
            title: this.buildTitle()
        }
    }
    
    // ...
}
