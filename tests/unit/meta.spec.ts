import { Component } from 'vue-property-decorator'
import { Meta } from '../../src/index'
import { expect } from 'chai'
import { mount, createLocalVue, Wrapper } from '@vue/test-utils'
import VueMeta from 'vue-meta'

const Vue = createLocalVue()
Vue.use(VueMeta)

@Component({ template: `<h1>foo</h1>` })
class Test extends Vue {
  @Meta
  metaConfig() {
    return { title: 'test' }
  }
}

describe('meta decorator', () => {
  it('sets metaInfo', () => {
    const test = mount(Test, { localVue: Vue })
    let info = test.vm.$options['metaInfo']
    info = info.call(test)
    
    expect(info.title).to.equal('test')
  })
})
